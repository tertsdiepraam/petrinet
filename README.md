# Remove Confusion from Occurrence Nets
This tool removes confusion from occurrence nets, which are read in as PNML or
the custom TOPL format. Additionally, it contains a folder of nets for testing
purposes. The program has been tested on Python 3.8.

The algorithm is based on a paper by Bruni, Melgratti and Montanari
([arXiv](https://arxiv.org/abs/1710.04570)). This project is part of the
bachelor thesis of Terts Diepraam, supervised by Lorenzo Galeotti.

The input path must be a path to a PNML or TOPL file. Positions must be
specified for all nodes. Any additional information, such as marking, in a PNML
file is discarded.

## Usage
The dependencies are NetworkX and Matplotlib. The versions that the program is
tested with are

```
networkx==2.4
matplotlib==3.2.1
```

These can be installed with

``` sh
pip install networkx matplotlib
```

Or, forcing the tested versions, with

``` sh
pip install networkx==2.4 matplotlib==3.2.1
```

The program can then be run with

``` sh
python main.py [input_path]
```

Several other scripts are available:
- `view.py` simply shows a Petri net,
- `scells.py` shows the structural branching cells identified by the program,
- `test_petri.py` runs the tests.

# Custom TOPL format
The TOPL format was created to quickly specify Petri nets. It requires much less
code than a similar PNML file, but is therefore also much less general.

An overview of the syntax is given below, creating a transition with two places
in both its preset and postset.

```
# Lines starting with a # are ignored
places:
a, (0,2)
b, (2,2)
c, (0,0)
d, (2,0)

transitions:
t, (1,1)

flow:
a, t
b, t
t, c
t, d
```

However, specifying the flow like this is often tedious, so there are a couple
shorter notations available. First, a chain of flow can be created on a single
line, such that the following is equivalent to the above:

```
flow:
a, t, c
b, t, d
```

Second, a list of nodes can be used to add flow between each combination. The following is again equivalent to the example above:

```
flow:
[a,b], t, [c,d]
```

# Acknowledgements
The `SharedMemory.pnml` file was created by Denis Poitrenaud and revised by
Fabrice Kordon.
