import pytest
from main import *


@pytest.fixture
def net1():
    net = PetriNet()
    net.add_places(
        {"id": "1", "pos": (1, 4)},
        {"id": "2", "pos": (0, 2)},
        {"id": "3", "pos": (1, 2)},
        {"id": "4", "pos": (0, 0)},
        {"id": "5", "pos": (1, 0)},
        {"id": "6", "pos": (2, 2)},
    )

    net.add_transitions(
        {"id": "a", "pos": (1, 3)},
        {"id": "b", "pos": (0, 1)},
        {"id": "c", "pos": (1, 1)},
        {"id": "d", "pos": (2, 3)},
    )

    net.add_flow(
        ("1", ["a", "d"]),
        ("a", "3"),
        ("d", "6"),
        ("3", "c"),
        ("2", ["b", "c"]),
        ("b", "4"),
        ("c", "5"),
    )

    return net


@pytest.fixture
def net2():
    net = PetriNet()
    net.add_places(
        {"id": "1", "pos": (1, 5)},
        {"id": "2", "pos": (0, 2)},
        {"id": "3", "pos": (1, 2)},
        {"id": "4", "pos": (0, 0)},
        {"id": "5", "pos": (1, 0)},
        {"id": "6", "pos": (2, 3)},
        {"id": "7", "pos": (3, 5)},
        {"id": "8", "pos": (3, 2)},
        {"id": "9", "pos": (4, 3)},
        {"id": "10", "pos": (3, 0)},
    )

    net.add_transitions(
        {"id": "a", "pos": (1, 4)},
        {"id": "b", "pos": (0, 1)},
        {"id": "c", "pos": (1, 1)},
        {"id": "d", "pos": (2, 3)},
        {"id": "e", "pos": (3, 4)},
        {"id": "f", "pos": (4, 4)},
        {"id": "g", "pos": (3, 1)},
    )

    net.add_flow(
        ("1", ["a", "d"]),
        ("7", ["e", "f"]),
        ("a", "3"),
        ("d", "6"),
        ("e", "8"),
        ("f", "9"),
        ("2", ["b", "c"]),
        ("3", "c"),
        ("8", ["c", "g"]),
        ("b", "4"),
        ("c", "5"),
        ("g", "10"),
    )

    return net


def test_scells(net1, net2):
    cells = {frozenset(s.graph.nodes) for s in net1.scells}
    assert cells == {
        frozenset(["1", "3", "6", "a", "d"]),
        frozenset(["2", "3", "4", "5", "b", "c"]),
    }

    cells = {frozenset(s.graph.nodes) for s in net2.scells}
    assert cells == {
        frozenset(["1", "3", "6", "a", "d"]),
        frozenset(["7", "8", "9", "e", "f"]),
        frozenset(["2", "3", "4", "5", "8", "10", "b", "c", "g"]),
    }


def test_postset(net1):
    assert net1.postset("1") == {"a", "d"}
    assert net1.postset("5") == set()
    assert net1.postset("3") == {"c"}


def test_preset(net1):
    assert net1.preset("1") == set()
    assert net1.preset("c") == {"2", "3"}
    assert net1.preset("5") == {"c"}


def test_minus(net1):
    n_minus_3 = net1.minus("3")
    assert set(n_minus_3.places) == {"1", "2", "4", "6"}
    assert set(n_minus_3.transitions) == {"a", "b", "d"}

    n_minus_1 = net1.minus("1")
    assert set(n_minus_1.places) == {"2", "4"}
    assert set(n_minus_1.transitions) == {"b"}


def test_initial(net1, net2):
    assert set(net1.initial) == {"1", "2"}
    assert set(net2.initial) == {"1", "2", "7"}


def test_final(net1, net2):
    assert set(net1.final) == {"4", "5", "6"}
    assert set(net2.final) == {"4", "5", "6", "9", "10"}


def test_transactions(net1):
    assert net1.transactions() == [
        ["a", "b"],
        ["a", "c"],
        ["b", "a"],
        ["b", "d"],
        ["d", "b"],
    ]
