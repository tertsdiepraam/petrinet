"""
Grammar:
Each line is either a node, an edge or a collection of edges.
There are places, transitions and flow keywords, which trigger different modes.
For places and transitions, each following line consists of an id, followed by a
comma and a position in parentheses. For flow each line consists of two entries
both of which could be either a single id or a list of ids in square brackets.
The carthesian product of the lists is added to the flow. Lines that start with
a hash are ignored.
"""
import petri


def parse_pos(s):
    lst, rest = parse_list(s, delim="()")
    return tuple(float(p) for p in lst), rest


def try_eval(s):
    try:
        return eval(s)
    except:
        return s


def parse_node(s):
    node_id, rest = tuple(elem.strip() for elem in s.split(",", 1))
    pos, rest = parse_pos(rest)
    if rest:
        kwargs = (x.split("=") for x in rest.split(","))
        kwargs = {key.strip(): try_eval(value.strip()) for key, value in kwargs}
    else:
        kwargs = {}
    return {"id": node_id, "pos": pos, **kwargs}


def parse_list(s, delim):
    if not s.startswith(delim[0]):
        raise ValueError(f"Expected {delim[0]}")

    lst, rest = s[1:].split(delim[1], 1)
    rest = rest.strip()
    if rest.startswith(","):
        rest = rest[1:].strip()
    elif rest:
        raise ValueError('Expected ","')
    return [elem.strip() for elem in lst.split(",")], rest


def parse_flow_component(s):
    if s.startswith("["):
        return parse_list(s, delim="[]")
    else:
        try:
            c, rest = s.split(",", 1)
        except ValueError:
            c, rest = s, ""
        return c.strip(), rest.strip()


def parse_lines(lines):
    mode = None
    net = petri.PetriNet()
    for i, line in enumerate(lines):
        line = line.strip()
        if not line or line.startswith("#"):
            continue

        if line.endswith(":"):
            if line[:-1] == "places":
                mode = "place"
            elif line[:-1] == "transitions":
                mode = "transition"
            elif line[:-1] == "flow":
                mode = "flow"
            else:
                raise ValueError(
                    f'{i}: Line ended with ":", but mode "{line[:-1]}" was not recognized.'
                )
            continue

        if mode == "place":
            net.add_places(parse_node(line))
        elif mode == "transition":
            net.add_transitions(parse_node(line))
        elif mode == "flow":
            components = []
            while line:
                component, line = parse_flow_component(line)
                components.append(component)
            net.add_flow(tuple(components))
    return net


def parse_file(path):
    with open(path) as f:
        return parse_lines(f.readlines())
