import argparse
from pathlib import Path
import matplotlib.pyplot as plt
import matplotlib
from petri import PetriNet

matplotlib.use("TkAgg")


def main(input_path):
    net = PetriNet(input_path)
    net.draw(scells=True)
    plt.show()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="View the scells in a Petri net."
    )
    parser.add_argument("input_path", type=Path)
    args = parser.parse_args()
    main(args.input_path)
