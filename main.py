import argparse
from pathlib import Path
import matplotlib.pyplot as plt
import matplotlib

from petri import PetriNet

matplotlib.use("TkAgg")

def main(input_path):
    net = PetriNet(input_path)
    removed = net.remove_confusion()
    removed.prune()
    removed.draw()
    plt.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Remove confusion from occurrence nets"
    )
    parser.add_argument("input_path", type=Path)
    args = parser.parse_args()
    main(args.input_path)
