import networkx as nx
import xml.etree.ElementTree as ET


def get_xmlns(el):
    if el.tag.startswith("{"):
        return el.tag[1:].split("}")[0]
    else:
        return ""


def parse_xy(el, default=None):
    """Extracts the x,y values from an xml element into a tuple."""
    if el is None:
        return default
    return (float(el.get("x")), float(el.get("y")))


def parse_file(f):
    """Parses a file in PNML format to a Networkx Graph."""
    tree = ET.parse(f)
    root = tree.getroot()

    nets = []

    ns = {"": get_xmlns(root)}
    def_ns = f"{{{ns['']}}}" if ns[""] else ""
    for net_node in root.iter(f"{def_ns}net"):
        net = nx.DiGraph()

        for i, transition_node in enumerate(net_node.iter(f"{def_ns}transition")):
            transition_id = transition_node.get("id") or f"t{i}"
            pos = parse_xy(transition_node.find("./graphics/position", ns))
            offset = parse_xy(
                transition_node.find("./graphics/offset", ns), default=(0, 0)
            )

            net.add_node(
                transition_id,
                node_type="transition",
                label=transition_node.find("./name/text", ns).text,
                pos=pos,
                offset=offset,
            )

        for i, place_node in enumerate(net_node.iter(f"{def_ns}place")):
            place_id = place_node.get(f"id") or f"p{i}"

            label = place_node.find("./name/text", ns).text
            pos = parse_xy(place_node.find("./graphics/position", ns))
            offset = parse_xy(place_node.find("./graphics/offset", ns), default=(0, 0))

            marking_node = place_node.find("./initialMarking/text", ns)
            marking = int(marking_node.text) if marking_node else 0

            net.add_node(
                place_id,
                node_type="place",
                label=label,
                pos=pos,
                offset=offset,
                marking=marking,
            )

        for i, arc_node in enumerate(net_node.iter(f"{def_ns}arc")):
            edge_id = arc_node.get("id") or f"a{i}"

            source = arc_node.get("source")
            target = arc_node.get("target")

            edge_type = arc_node.get(f"{def_ns}type")
            if edge_type is None:
                etp = arc_node.find("./type", ns)
                if etp is not None:
                    edge_type = etp.get(f"{def_ns}value")
                if edge_type is None:
                    edge_type = "normal"

            inscr_txt = arc_node.find("./inscription/text", ns)
            inscription = inscr_txt.text if inscr_txt else None

            net.add_edge(
                source, target, id=edge_id, type=edge_type, inscription=inscription,
            )

        nets.append(net)

    return nets
