import petri
import matplotlib.pyplot as plt
import sys
from pathlib import Path

net = petri.PetriNet(Path(sys.argv[1]))
net.draw()
plt.show()
