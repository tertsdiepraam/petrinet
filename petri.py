import itertools
import networkx as nx
import pnml
import topl
from pprint import pprint


class PetriNet:
    def __init__(self, input_file=None):
        if input_file:
            if input_file.suffix == ".pnml":
                self.graph = pnml.parse_file(input_file)[0]
            elif input_file.suffix == ".topl":
                self.graph = topl.parse_file(input_file).graph
            else:
                raise ValueError(
                    f'File extension "{input_file.suffix}" was not recognized.'
                )
        else:
            self.graph = nx.DiGraph()

    @property
    def places(self):
        return (n for n, d in self.graph.nodes(data=True) if d["node_type"] == "place")

    @property
    def transitions(self):
        return (
            n for n, d in self.graph.nodes(data=True) if d["node_type"] == "transition"
        )

    @property
    def initial(self):
        return {p for p in self.places if not self.preset(p)}

    @property
    def final(self):
        return {p for p in self.places if not self.postset(p)}

    def add_places(self, *p_dicts):
        num_places = sum(1 for _ in self.places)
        for i, p in enumerate(p_dicts, start=num_places):
            id = p.get("id", f"p{i}")
            self.graph.add_node(id, node_type="place", **p)

    def add_transitions(self, *t_dicts):
        num_places = sum(1 for _ in self.transitions)
        for i, t in enumerate(t_dicts, start=num_places):
            id = t.get("id", f"t{i}")
            self.graph.add_node(id, node_type="transition", **t)

    def add_flow(self, *flows):
        for flow in flows:
            for a, b in zip(flow[:-1], flow[1:]):
                if type(a) not in [list, set, tuple, frozenset]:
                    a = [a]
                if type(b) not in [list, set, tuple, frozenset]:
                    b = [b]
                if "tnot c1" in a or "tnot c1" in b:
                    raise ValueError("Boom")
                self.graph.add_edges_from(itertools.product(a, b))

    def preset(self, a):
        return set(self.graph.predecessors(a))

    def postset(self, a):
        return set(self.graph.successors(a))

    def enabled_transitions(self, marking):
        return (t for t in self.transitions if self.preset(t) <= marking)

    def direct_conflict(self, a, b):
        """Models the #_0 relation from Abbes & Benveniste."""
        return bool(self.preset(a) & self.preset(b))

    def node_type(self, a):
        return self.graph.nodes[a]["node_type"]

    def scell(self, a):
        cell = set()
        queue = [a]
        transitions = []
        for elem in queue:
            if elem not in cell:
                cell.add(elem)
                if self.node_type(elem) == "place":
                    queue.extend(self.postset(elem))
                elif self.node_type(elem) == "transition":
                    queue.extend(self.preset(elem))
                    transitions.append(elem)

        # Add the final places
        for t in transitions:
            cell |= self.postset(t)
        return self.restrict_to(cell)

    @property
    def scells(self):
        copy = self.graph.copy()
        transitions = list(self.transitions)
        places = list(self.places)
        copy.add_edges_from((t, p) for p,t in self.graph.edges() if t in transitions and p in places)
        print(list(nx.strongly_connected_components(copy)))
        return nx.strongly_connected_components(copy)

    def restrict_to(self, nodes):
        net = PetriNet()
        postsets = {p for t in set(self.transitions) & nodes for p in self.postset(t)}
        net.graph = self.graph.subgraph(nodes | postsets)
        return net

    def minus(self, start_node):
        """Models the \\ominus operation from Abbes & Benveniste."""
        removal_candidates = [start_node]
        to_be_removed = set()
        for n in removal_candidates:
            if n in to_be_removed:
                continue

            node_type = self.graph.nodes[n]["node_type"]
            if node_type == "place":
                if n == start_node or all((p in to_be_removed for p in self.preset(n))):
                    to_be_removed.add(n)
                    removal_candidates.extend(self.postset(n))
            elif node_type == "transition":
                to_be_removed.add(n)
                removal_candidates.extend(self.postset(n))
            else:
                raise ValueError(f'Node type "{node_type}" not recognized')

        nodes = set(self.graph.nodes) - to_be_removed
        return self.restrict_to(nodes)

    def transactions(self, marking=None):
        """Transactions are given as a tuple containing the id of the first
        transition and a set of all transitions in the transaction."""
        if marking is None:
            marking = self.initial

        transactions = {}
        for t in self.enabled_transitions(marking):
            nested_transactions = self.transactions(
                self.fire(marking, t)
            )
            if not nested_transactions:
                transactions[frozenset([t])] = t
            else:
                # The first element of the nested_transactions is discarded
                # because we're adding a new first element.
                for _, transaction in nested_transactions:
                    transactions[transaction | {t}] = t

        return [(first, transaction) for transaction, first in transactions.items()]

    def empty(self):
        return len(self.graph) == 0

    def fire(self, marking, *transitions):
        for t in transitions:
            marking = marking - self.preset(t) | self.postset(t)
        return marking

    def remove_confusion(self):
        cells = []
        for scell in self.scells:
            scell = self.restrict_to(scell)
            cells.append(scell.remove_confusion_scell())
        return compose(*cells)

    def remove_confusion_scell(self):
        new_cell = PetriNet()
        # Determine transactions
        # Find any nested scells
        # The nested cells should act on a copy of the interal transitions
        # and places but the same initial and final places.

        # For all inputs and outputs there should be a negative version
        # All negative inputs are connected to a new transition
        for i in self.initial:
            data = self.graph.nodes[i]
            pos = data["pos"]
            not_id = "not " + data["id"]
            new_cell.add_places(
                {"id": data["id"], "pos": data["pos"]},
                {"id": not_id, "pos": (pos[0] + 0.5, pos[1])},
                {"id": "p" + not_id, "pos": (pos[0] + 0.25, pos[1] - 0.5)}
            )
            new_cell.add_transitions(
                {"id": "t" + not_id, "pos": (pos[0] + 0.5, pos[1] - 1)}
            )
            new_cell.add_flow(
                (not_id, "t" + not_id),
                ("t" + not_id, "p" + not_id)
            )

        # Adding negative versions of all final places
        for f in self.final:
            data = self.graph.nodes[f]
            pos = data["pos"]
            not_id = "not " + data["id"]
            new_cell.add_places(
                {"id": data["id"], "pos": data["pos"]},
                {"id": not_id, "pos": (pos[0] + 0.5, pos[1])}
            )

        # All transactions should connect to all positive initial and final
        # places.
        for first, transaction in self.transactions():
            transition_id = ",".join(transaction)
            new_cell.add_transitions(
                {"id": transition_id, "pos": self.graph.nodes[first]['pos']}
            )
            marked_after_execution = self.final & self.fire(self.initial, *transaction)
            negatives = {"not " + s for s in self.final - marked_after_execution}
            new_cell.add_flow(
                (self.initial, transition_id),
                (transition_id, marked_after_execution),
                (transition_id, negatives),
            )

        for p in self.initial:
            subcell = self.minus(p)
            if set(subcell.transitions):
                unreachable_final_places = ["not " + q for q in self.final - subcell.final]
                new_cell.add_flow(
                    ("tnot " + p, unreachable_final_places)
                )
                new_subcell = subcell.remove_confusion_scell()
                relabeling = {}
                for t in new_subcell.transitions:
                    if t.startswith('tnot'):
                        continue
                    data = new_subcell.graph.nodes[t]
                    pos = data['pos']
                    new_id = f"{data['id']}-{p}"
                    relabeling[data['id']] = new_id
                    data['id'] = new_id
                    data['pos'] = (pos[0]+0.25, pos[1])

                nx.relabel_nodes(new_subcell.graph, relabeling, copy=False)

                new_cell = compose(new_cell, new_subcell)

                new_cell.add_flow(
                    ("pnot " + p, list(filter(lambda x: not x.startswith('tnot'), new_subcell.transitions)))
                )

        return new_cell

    def prune(self, initial=True, final=True, shortcuts=True):
        if initial:
            for p in self.initial:
                if p.startswith("not"):
                    self.graph = nx.DiGraph(self.minus(p).graph)

        if final:
            for p in self.final:
                if p.startswith("not") or p.startswith("pnot") or p.startswith("tnot"):
                    self.graph = nx.DiGraph(self.minus(p).graph)

        if shortcuts:
            for p in list(self.places):
                if p not in self.graph:
                    continue
                if not p.startswith("not"):
                    continue
                if self.postset(p) == {"t" + p} and self.postset("t" + p) == {"p" + p}:
                    self.add_flow(
                        (p, self.postset("p" + p))
                    )
                    self.graph.remove_node("t" + p)
                    self.graph.remove_node("p" + p)


    def draw(self, scells=False):
        positions = {n: d["pos"] for n, d in self.graph.nodes(data=True)}
        labels = {n: d["id"] for n, d in self.graph.nodes(data=True)}
        places = list(self.places)
        transitions = list(self.transitions)
        p_color_map, t_color_map = None, None
       
        if scells:
            scells_dict = {label:i for i, c in enumerate(self.scells) for label in c}
            p_color_map = [scells_dict[p] for p in places]
            t_color_map = [scells_dict[t] for t in transitions]
            vmin = min(*p_color_map, *t_color_map)
            vmax = max(*p_color_map, *t_color_map)

            nx.draw_networkx_nodes(
                self.graph, positions, node_shape="o",
                nodelist=places, node_color=p_color_map,
                vmin=vmin, vmax=vmax
            )
            nx.draw_networkx_nodes(
                self.graph, positions, node_shape="s",
                nodelist=transitions, node_color=t_color_map,
                vmin=vmin, vmax=vmax
            )
        else:
            nx.draw_networkx_nodes(
                self.graph, positions, node_shape="o", nodelist=places
            )
            nx.draw_networkx_nodes(
                self.graph, positions, node_shape="s", nodelist=transitions
            )

        nx.draw_networkx_edges(self.graph, positions)
        nx.draw_networkx_labels(self.graph, positions)


def compose(*cells):
    net = PetriNet()
    for cell in cells:
        net.graph = nx.compose(net.graph, cell.graph)
    return net
